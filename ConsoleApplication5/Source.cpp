#include <windows.h>
#include <fstream>
#include <math.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <locale.h>
#include <iostream>

#if (NTDDI_VERSION >= NTDDI_WIN8)
typedef MFT_ENUM_DATA_V1 MFT_ENUM_DATA, *PMFT_ENUM_DATA;
#else
typedef MFT_ENUM_DATA_V0 MFT_ENUM_DATA, *PMFT_ENUM_DATA;
#endif

using namespace std;
												   
struct Starting
{
public:
	DWORD Size;
	LPWORD StartingVcn;
public:
	Starting()
	{
		Size = sizeof(STARTING_VCN_INPUT_BUFFER);
	}
};

struct Retrieval
{
public:
	DWORD Size;
	DWORD ExtentCount = 0; //����� ��������� � �������
	LPWORD StartingVcn = 0;
	LPWORD NextVcn = 0;
	LPWORD Lcn = 0;
public:
	Retrieval()
	{
		Size = sizeof(RETRIEVAL_POINTERS_BUFFER);
	};
};


int main() {
	LPCSTR  lpRootPathName;
	LPDWORD lpSectorsPerCluster = 0;
	LPDWORD lpBytesPerSector = 0;
	LPDWORD lpNumberOfFreeClusters = 0;
	LPDWORD lpTotalNumberOfClusters = 0;							 

	bool check = GetDiskFreeSpaceA("C:\\", (LPDWORD)&lpSectorsPerCluster, (LPDWORD)&lpBytesPerSector, (LPDWORD)&lpNumberOfFreeClusters, (LPDWORD)&lpTotalNumberOfClusters);

	char string[20] = "";
	_itoa_s((int)lpNumberOfFreeClusters, string, 20, 10);

	std::cout << "Sectors Per Cluster: " << lpSectorsPerCluster << endl;
	std::cout << "Bytes Per Sector: " << lpBytesPerSector << endl;
	std::cout << "Number Of Free Clusters: " << string << endl;
	_itoa_s((int)lpTotalNumberOfClusters, string, 20, 10);
	std::cout << "Total Number Of Clusters: " << string << endl;

	HANDLE hDisk = CreateFile(L"\\\\.\\PhysicalDrive0", GENERIC_WRITE | GENERIC_READ,
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	if (hDisk == INVALID_HANDLE_VALUE) {
		printf("error");
		_getch();
		return 0;
	}

	long maxClusters = 0;
	Starting* vcnIn = (Starting*)malloc(sizeof(Starting));
	Retrieval* rpb = (Retrieval*)malloc(sizeof(Retrieval));
	DWORD bytesReturned = 0;
	int err = 0;
	long totalClusters = 0;
	int extentNumber = 1;

	vcnIn->StartingVcn = 0L;
	
	DWORD nInBufferSize = 0;
	DWORD nOutBufferSize = 0;

	DWORD cb;


	//����� ����� �  ���������������� �������� VCN. ���������, ����� ��� �������� �� ������������� 234
	do								
	{
		DeviceIoControl(
			hDisk,
			FSCTL_GET_RETRIEVAL_POINTERS,
			&vcnIn,
			sizeof(vcnIn),
			&rpb,
			sizeof(rpb),
			&bytesReturned,
			NULL
		);

		//��������� ������ Win32
		err = GetLastError();
		//err = 0;
		switch (err)
		{
		case 38: { // ERROR_HANDLE_EOF
			if (extentNumber == 1) {
				cout << "MFT Table...";
			}
			else
				cout <<
				"\r\nTotal clusters:" << totalClusters << "\r\nFragments: " << extentNumber - 1 << endl;
			maxClusters = totalClusters;

			break;
		}
			case 0: // NO_ERROR

				cout << "Fragment #0" << extentNumber;
				cout << "\tStart cluster: " << rpb->Lcn << "\r\n\tLength: " << rpb->NextVcn - rpb->StartingVcn << "clusters" << endl;
				cout << "\r\nTotal clusters: " << rpb->NextVcn - rpb->StartingVcn << "\r\nFragments: " << extentNumber << endl;
				if (rpb->NextVcn - rpb->StartingVcn >= maxClusters)
				{
					maxClusters = rpb->NextVcn - rpb->StartingVcn;
				}

				break;
			case 234: // ERROR_MORE_DATA

				cout << "Fragment #" << extentNumber++ << endl;
				cout << "\tStart cluster: " << rpb->Lcn << "\r\n\tLength: " << rpb->NextVcn - rpb->StartingVcn << "clusters" << endl;

				totalClusters += rpb->NextVcn - rpb->StartingVcn;
				vcnIn->StartingVcn = rpb->NextVcn;
			break;
		default:
			continue;
			break;
		}
	} while (err == 234);

	system("pause");
	return 0;
}